﻿
Namespace My
    
    'This class allows you to handle specific events on the settings class:
    ' The SettingChanging event is raised before a setting's value is changed.
    ' The PropertyChanged event is raised after a setting's value is changed.
    ' The SettingsLoaded event is raised after the setting values are loaded.
    ' The SettingsSaving event is raised before the setting values are saved.
    Partial Friend NotInheritable Class MySettings
        Public m2mConnection As String = ""
        Public psConnection As String = ""

        Private Sub MySettings_SettingsLoaded(ByVal sender As Object, ByVal e As System.Configuration.SettingsLoadedEventArgs) Handles Me.SettingsLoaded
            If sconnM2M.Length > 0 Then
                Me.Item("m2mdata01ConnectionString") = sconnM2M
            End If
            If sconnPackingSolution.Length > 0 Then
                Me.Item("Packing_SolutionConnectionString") = sconnPackingSolution
            End If
        End Sub

        Private Sub MySettings_SettingsSaving(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.SettingsSaving
            If m2mConnection.Length > 0 Then
                Me.Item("m2mdata01ConnectionString") = m2mConnection
                Me.sconnM2M = m2mConnection
            End If
            If psConnection.Length > 0 Then
                Me.Item("Packing_SolutionConnectionString") = psConnection
                Me.sconnPackingSolution = psConnection
            End If
        End Sub
    End Class
End Namespace
