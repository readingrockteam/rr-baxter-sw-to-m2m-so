﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSWtoM2M
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Fkey_idLabel As System.Windows.Forms.Label
        Dim FnotesLabel As System.Windows.Forms.Label
        Dim FmstreetLabel As System.Windows.Forms.Label
        Dim FccompanyLabel As System.Windows.Forms.Label
        Dim ColorLabel As System.Windows.Forms.Label
        Dim lblTotSelected As System.Windows.Forms.Label
        Dim lblProjSelected As System.Windows.Forms.Label
        Dim Timestamp_ColumnLabel As System.Windows.Forms.Label
        Dim lblShippingAddNum As System.Windows.Forms.Label
        Dim lblPM As System.Windows.Forms.Label
        Dim FcustnoLabel As System.Windows.Forms.Label
        Dim FpoamtLabel As System.Windows.Forms.Label
        Dim FsonoLabel As System.Windows.Forms.Label
        Dim FprojectLabel As System.Windows.Forms.Label
        Dim FFMIDLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSWtoM2M))
        Me.FMAKERIDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsSWM2M = New SW_to_M2M_SO.dsSWM2M
        Me.LeadtimesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SomastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ReconfigDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.M2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PackingSolutionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutSWToM2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Rockcast_colorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SyaddrBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sw_releaseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.M2mFac_ps_xrefBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SoitemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FMAKERIDTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.FMAKERIDTableAdapter
        Me.TableAdapterManager = New SW_to_M2M_SO.dsSWM2MTableAdapters.TableAdapterManager
        Me.InmastTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.inmastTableAdapter
        Me.InvcurTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.invcurTableAdapter
        Me.SlcdpmTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.slcdpmTableAdapter
        Me.SoitemTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.soitemTableAdapter
        Me.SOMAST_EXTTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.SOMAST_EXTTableAdapter
        Me.SomastTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.somastTableAdapter
        Me.SorelsTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.sorelsTableAdapter
        Me.SotaxTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.sotaxTableAdapter
        Me.SyaddrTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.syaddrTableAdapter
        Me.SwmastTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.swmastTableAdapter
        Me.InmastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Rockcast_colorsTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.rockcast_colorsTableAdapter
        Me.InvcurBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SlcdpmBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SorelsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SotaxBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SOMAST_EXTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.All_so_qtysBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.All_so_qtysTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.all_so_qtysTableAdapter
        Me.Email_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Email_listTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.email_listTableAdapter
        Me.SwmastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sw_releaseTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.sw_releaseTableAdapter
        Me.M2mFac_ps_xrefTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.m2mFac_ps_xrefTableAdapter
        Me.App_errorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.App_errorsTableAdapter = New SW_to_M2M_SO.dsSWM2MTableAdapters.app_errorsTableAdapter
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Fkey_idComboBox = New System.Windows.Forms.ComboBox
        Me.cmdUpdate = New System.Windows.Forms.Button
        Me.cbxRelease = New System.Windows.Forms.ComboBox
        Me.FnotesTextBox = New System.Windows.Forms.TextBox
        Me.fcustno = New System.Windows.Forms.Label
        Me.FcaddrkeyComboBox = New System.Windows.Forms.ComboBox
        Me.FmstreetLabel1 = New System.Windows.Forms.Label
        Me.FccompanyLabel1 = New System.Windows.Forms.Label
        Me.ColorComboBox = New System.Windows.Forms.ComboBox
        Me.lblDraftRel = New System.Windows.Forms.Label
        Me.lblFFMID = New System.Windows.Forms.Label
        Me.lblPoAmt = New System.Windows.Forms.Label
        Me.lblselectcuft = New System.Windows.Forms.Label
        Me.lblprojcuft = New System.Windows.Forms.Label
        Me.FprojectTextBox = New System.Windows.Forms.TextBox
        Me.txtPM = New System.Windows.Forms.TextBox
        Me.Timestamp_ColumnLabel1 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.cmdLastSo = New System.Windows.Forms.Button
        Me.FsonoTextBox = New System.Windows.Forms.TextBox
        Me.cbxAbcCode = New System.Windows.Forms.ComboBox
        Me.lblABC = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblCpcft = New System.Windows.Forms.Label
        Me.txtCpcft = New System.Windows.Forms.TextBox
        Me.FFMIDComboBox = New System.Windows.Forms.ComboBox
        Me.cmdAddSo = New System.Windows.Forms.Button
        Me.cmdLoadInmast = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.SwmastDataGridView = New System.Windows.Forms.DataGridView
        Me.dgAdd = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgPieceMark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgQuoteQty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgStarted = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgOpen = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgClosed = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgOnHold = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgCancelled = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgShipped = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgAddQty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgFnotes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgCUFT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgRelease = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgFpartno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgOrdQty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FfmidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SysdateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdentityColumnDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgDrawno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.verified = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.sawcut = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.extract_path = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Fkey_idLabel = New System.Windows.Forms.Label
        FnotesLabel = New System.Windows.Forms.Label
        FmstreetLabel = New System.Windows.Forms.Label
        FccompanyLabel = New System.Windows.Forms.Label
        ColorLabel = New System.Windows.Forms.Label
        lblTotSelected = New System.Windows.Forms.Label
        lblProjSelected = New System.Windows.Forms.Label
        Timestamp_ColumnLabel = New System.Windows.Forms.Label
        lblShippingAddNum = New System.Windows.Forms.Label
        lblPM = New System.Windows.Forms.Label
        FcustnoLabel = New System.Windows.Forms.Label
        FpoamtLabel = New System.Windows.Forms.Label
        FsonoLabel = New System.Windows.Forms.Label
        FprojectLabel = New System.Windows.Forms.Label
        FFMIDLabel = New System.Windows.Forms.Label
        CType(Me.FMAKERIDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsSWM2M, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeadtimesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SomastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.Rockcast_colorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SyaddrBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sw_releaseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.M2mFac_ps_xrefBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SoitemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InmastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvcurBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SlcdpmBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SorelsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SotaxBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SOMAST_EXTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.All_so_qtysBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Email_listBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SwmastBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.App_errorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.SwmastDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Fkey_idLabel
        '
        Fkey_idLabel.AutoSize = True
        Fkey_idLabel.Location = New System.Drawing.Point(36, 6)
        Fkey_idLabel.Name = "Fkey_idLabel"
        Fkey_idLabel.Size = New System.Drawing.Size(54, 13)
        Fkey_idLabel.TabIndex = 91
        Fkey_idLabel.Text = "MFG Fac:"
        '
        'FnotesLabel
        '
        FnotesLabel.AutoSize = True
        FnotesLabel.Location = New System.Drawing.Point(669, 158)
        FnotesLabel.Name = "FnotesLabel"
        FnotesLabel.Size = New System.Drawing.Size(77, 13)
        FnotesLabel.TabIndex = 88
        FnotesLabel.Text = "Filter by Notes:"
        '
        'FmstreetLabel
        '
        FmstreetLabel.AutoSize = True
        FmstreetLabel.Location = New System.Drawing.Point(553, 114)
        FmstreetLabel.Name = "FmstreetLabel"
        FmstreetLabel.Size = New System.Drawing.Size(38, 13)
        FmstreetLabel.TabIndex = 83
        FmstreetLabel.Text = "Street:"
        '
        'FccompanyLabel
        '
        FccompanyLabel.AutoSize = True
        FccompanyLabel.Location = New System.Drawing.Point(537, 90)
        FccompanyLabel.Name = "FccompanyLabel"
        FccompanyLabel.Size = New System.Drawing.Size(54, 13)
        FccompanyLabel.TabIndex = 82
        FccompanyLabel.Text = "Company:"
        '
        'ColorLabel
        '
        ColorLabel.AutoSize = True
        ColorLabel.Location = New System.Drawing.Point(52, 98)
        ColorLabel.Name = "ColorLabel"
        ColorLabel.Size = New System.Drawing.Size(34, 13)
        ColorLabel.TabIndex = 78
        ColorLabel.Text = "Color:"
        '
        'lblTotSelected
        '
        lblTotSelected.AutoSize = True
        lblTotSelected.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblTotSelected.Location = New System.Drawing.Point(803, 28)
        lblTotSelected.Name = "lblTotSelected"
        lblTotSelected.Size = New System.Drawing.Size(141, 17)
        lblTotSelected.TabIndex = 73
        lblTotSelected.Text = "Total CUFT selected:"
        '
        'lblProjSelected
        '
        lblProjSelected.AutoSize = True
        lblProjSelected.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblProjSelected.Location = New System.Drawing.Point(792, 2)
        lblProjSelected.Name = "lblProjSelected"
        lblProjSelected.Size = New System.Drawing.Size(152, 17)
        lblProjSelected.TabIndex = 71
        lblProjSelected.Text = "Total CUFT on Project:"
        '
        'Timestamp_ColumnLabel
        '
        Timestamp_ColumnLabel.AutoSize = True
        Timestamp_ColumnLabel.Location = New System.Drawing.Point(160, 178)
        Timestamp_ColumnLabel.Name = "Timestamp_ColumnLabel"
        Timestamp_ColumnLabel.Size = New System.Drawing.Size(99, 13)
        Timestamp_ColumnLabel.TabIndex = 72
        Timestamp_ColumnLabel.Text = "Timestamp Column:"
        Timestamp_ColumnLabel.Visible = False
        '
        'lblShippingAddNum
        '
        lblShippingAddNum.AutoSize = True
        lblShippingAddNum.Location = New System.Drawing.Point(506, 61)
        lblShippingAddNum.Name = "lblShippingAddNum"
        lblShippingAddNum.Size = New System.Drawing.Size(88, 13)
        lblShippingAddNum.TabIndex = 69
        lblShippingAddNum.Text = "Address Number:"
        '
        'lblPM
        '
        lblPM.AutoSize = True
        lblPM.Location = New System.Drawing.Point(536, 158)
        lblPM.Name = "lblPM"
        lblPM.Size = New System.Drawing.Size(58, 13)
        lblPM.TabIndex = 68
        lblPM.Text = "PM Initials:"
        '
        'FcustnoLabel
        '
        FcustnoLabel.AutoSize = True
        FcustnoLabel.Location = New System.Drawing.Point(543, 30)
        FcustnoLabel.Name = "FcustnoLabel"
        FcustnoLabel.Size = New System.Drawing.Size(48, 13)
        FcustnoLabel.TabIndex = 70
        FcustnoLabel.Text = "Cust No:"
        '
        'FpoamtLabel
        '
        FpoamtLabel.AutoSize = True
        FpoamtLabel.Location = New System.Drawing.Point(37, 79)
        FpoamtLabel.Name = "FpoamtLabel"
        FpoamtLabel.Size = New System.Drawing.Size(46, 13)
        FpoamtLabel.TabIndex = 67
        FpoamtLabel.Text = "PO Amt:"
        '
        'FsonoLabel
        '
        FsonoLabel.AutoSize = True
        FsonoLabel.Location = New System.Drawing.Point(534, 6)
        FsonoLabel.Name = "FsonoLabel"
        FsonoLabel.Size = New System.Drawing.Size(57, 13)
        FsonoLabel.TabIndex = 65
        FsonoLabel.Text = "New SO#:"
        '
        'FprojectLabel
        '
        FprojectLabel.AutoSize = True
        FprojectLabel.Location = New System.Drawing.Point(45, 59)
        FprojectLabel.Name = "FprojectLabel"
        FprojectLabel.Size = New System.Drawing.Size(38, 13)
        FprojectLabel.TabIndex = 61
        FprojectLabel.Text = "Name:"
        '
        'FFMIDLabel
        '
        FFMIDLabel.AutoSize = True
        FFMIDLabel.Location = New System.Drawing.Point(41, 32)
        FFMIDLabel.Name = "FFMIDLabel"
        FFMIDLabel.Size = New System.Drawing.Size(42, 13)
        FFMIDLabel.TabIndex = 60
        FFMIDLabel.Text = "FFMID:"
        '
        'FMAKERIDBindingSource
        '
        Me.FMAKERIDBindingSource.DataMember = "FMAKERID"
        Me.FMAKERIDBindingSource.DataSource = Me.DsSWM2M
        '
        'DsSWM2M
        '
        Me.DsSWM2M.DataSetName = "dsSWM2M"
        Me.DsSWM2M.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LeadtimesBindingSource
        '
        Me.LeadtimesBindingSource.DataMember = "lead_times"
        Me.LeadtimesBindingSource.DataSource = Me.DsSWM2M
        '
        'SomastBindingSource
        '
        Me.SomastBindingSource.DataMember = "somast"
        Me.SomastBindingSource.DataSource = Me.DsSWM2M
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AdminToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1234, 24)
        Me.MenuStrip1.TabIndex = 36
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReconfigDBToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'ReconfigDBToolStripMenuItem
        '
        Me.ReconfigDBToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.M2MToolStripMenuItem, Me.PackingSolutionToolStripMenuItem})
        Me.ReconfigDBToolStripMenuItem.Name = "ReconfigDBToolStripMenuItem"
        Me.ReconfigDBToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ReconfigDBToolStripMenuItem.Text = "Reconfig DB"
        '
        'M2MToolStripMenuItem
        '
        Me.M2MToolStripMenuItem.Name = "M2MToolStripMenuItem"
        Me.M2MToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.M2MToolStripMenuItem.Text = "M2M"
        '
        'PackingSolutionToolStripMenuItem
        '
        Me.PackingSolutionToolStripMenuItem.Name = "PackingSolutionToolStripMenuItem"
        Me.PackingSolutionToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.PackingSolutionToolStripMenuItem.Text = "Packing Solution"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutSWToM2MToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'AboutSWToM2MToolStripMenuItem
        '
        Me.AboutSWToM2MToolStripMenuItem.Name = "AboutSWToM2MToolStripMenuItem"
        Me.AboutSWToM2MToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.AboutSWToM2MToolStripMenuItem.Text = "About SW to M2M"
        '
        'Rockcast_colorsBindingSource
        '
        Me.Rockcast_colorsBindingSource.DataMember = "rockcast_colors"
        Me.Rockcast_colorsBindingSource.DataSource = Me.DsSWM2M
        '
        'SyaddrBindingSource
        '
        Me.SyaddrBindingSource.DataMember = "syaddr"
        Me.SyaddrBindingSource.DataSource = Me.DsSWM2M
        '
        'Sw_releaseBindingSource
        '
        Me.Sw_releaseBindingSource.DataMember = "sw_release"
        Me.Sw_releaseBindingSource.DataSource = Me.DsSWM2M
        '
        'M2mFac_ps_xrefBindingSource
        '
        Me.M2mFac_ps_xrefBindingSource.DataMember = "m2mFac_ps_xref"
        Me.M2mFac_ps_xrefBindingSource.DataSource = Me.DsSWM2M
        '
        'SoitemBindingSource
        '
        Me.SoitemBindingSource.DataMember = "soitem"
        Me.SoitemBindingSource.DataSource = Me.DsSWM2M
        '
        'FMAKERIDTableAdapter
        '
        Me.FMAKERIDTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.app_errorsTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.email_listTableAdapter = Nothing
        Me.TableAdapterManager.FMAKERIDTableAdapter = Nothing
        Me.TableAdapterManager.inmastTableAdapter = Me.InmastTableAdapter
        Me.TableAdapterManager.invcurTableAdapter = Me.InvcurTableAdapter
        Me.TableAdapterManager.m2mFac_ps_xrefTableAdapter = Nothing
        Me.TableAdapterManager.rockcast_colorsTableAdapter = Nothing
        Me.TableAdapterManager.slcdpmTableAdapter = Me.SlcdpmTableAdapter
        Me.TableAdapterManager.SOITEM_EXTTableAdapter = Nothing
        Me.TableAdapterManager.soitemTableAdapter = Me.SoitemTableAdapter
        Me.TableAdapterManager.SOMAST_EXTTableAdapter = Me.SOMAST_EXTTableAdapter
        Me.TableAdapterManager.somastTableAdapter = Me.SomastTableAdapter
        Me.TableAdapterManager.sorelsTableAdapter = Me.SorelsTableAdapter
        Me.TableAdapterManager.sotaxTableAdapter = Me.SotaxTableAdapter
        Me.TableAdapterManager.sw_to_m2m_soTableAdapter = Nothing
        Me.TableAdapterManager.swmastTableAdapter = Nothing
        Me.TableAdapterManager.syaddrTableAdapter = Me.SyaddrTableAdapter
        Me.TableAdapterManager.UpdateOrder = SW_to_M2M_SO.dsSWM2MTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'InmastTableAdapter
        '
        Me.InmastTableAdapter.ClearBeforeFill = True
        '
        'InvcurTableAdapter
        '
        Me.InvcurTableAdapter.ClearBeforeFill = True
        '
        'SlcdpmTableAdapter
        '
        Me.SlcdpmTableAdapter.ClearBeforeFill = True
        '
        'SoitemTableAdapter
        '
        Me.SoitemTableAdapter.ClearBeforeFill = True
        '
        'SOMAST_EXTTableAdapter
        '
        Me.SOMAST_EXTTableAdapter.ClearBeforeFill = True
        '
        'SomastTableAdapter
        '
        Me.SomastTableAdapter.ClearBeforeFill = True
        '
        'SorelsTableAdapter
        '
        Me.SorelsTableAdapter.ClearBeforeFill = True
        '
        'SotaxTableAdapter
        '
        Me.SotaxTableAdapter.ClearBeforeFill = True
        '
        'SyaddrTableAdapter
        '
        Me.SyaddrTableAdapter.ClearBeforeFill = True
        '
        'SwmastTableAdapter
        '
        Me.SwmastTableAdapter.ClearBeforeFill = True
        '
        'InmastBindingSource
        '
        Me.InmastBindingSource.DataMember = "inmast"
        Me.InmastBindingSource.DataSource = Me.DsSWM2M
        '
        'Rockcast_colorsTableAdapter
        '
        Me.Rockcast_colorsTableAdapter.ClearBeforeFill = True
        '
        'InvcurBindingSource
        '
        Me.InvcurBindingSource.DataMember = "invcur"
        Me.InvcurBindingSource.DataSource = Me.DsSWM2M
        '
        'SlcdpmBindingSource
        '
        Me.SlcdpmBindingSource.DataMember = "slcdpm"
        Me.SlcdpmBindingSource.DataSource = Me.DsSWM2M
        '
        'SorelsBindingSource
        '
        Me.SorelsBindingSource.DataMember = "sorels"
        Me.SorelsBindingSource.DataSource = Me.DsSWM2M
        '
        'SotaxBindingSource
        '
        Me.SotaxBindingSource.DataMember = "sotax"
        Me.SotaxBindingSource.DataSource = Me.DsSWM2M
        '
        'SOMAST_EXTBindingSource
        '
        Me.SOMAST_EXTBindingSource.DataMember = "SOMAST_EXT"
        Me.SOMAST_EXTBindingSource.DataSource = Me.DsSWM2M
        '
        'All_so_qtysBindingSource
        '
        Me.All_so_qtysBindingSource.DataMember = "all_so_qtys"
        Me.All_so_qtysBindingSource.DataSource = Me.DsSWM2M
        '
        'All_so_qtysTableAdapter
        '
        Me.All_so_qtysTableAdapter.ClearBeforeFill = True
        '
        'Email_listBindingSource
        '
        Me.Email_listBindingSource.DataMember = "email_list"
        Me.Email_listBindingSource.DataSource = Me.DsSWM2M
        '
        'Email_listTableAdapter
        '
        Me.Email_listTableAdapter.ClearBeforeFill = True
        '
        'SwmastBindingSource
        '
        Me.SwmastBindingSource.DataMember = "swmast"
        Me.SwmastBindingSource.DataSource = Me.DsSWM2M
        '
        'Sw_releaseTableAdapter
        '
        Me.Sw_releaseTableAdapter.ClearBeforeFill = True
        '
        'M2mFac_ps_xrefTableAdapter
        '
        Me.M2mFac_ps_xrefTableAdapter.ClearBeforeFill = True
        '
        'App_errorsBindingSource
        '
        Me.App_errorsBindingSource.DataMember = "app_errors"
        Me.App_errorsBindingSource.DataSource = Me.DsSWM2M
        '
        'App_errorsTableAdapter
        '
        Me.App_errorsTableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Fkey_idLabel)
        Me.Panel2.Controls.Add(Me.Fkey_idComboBox)
        Me.Panel2.Controls.Add(Me.cmdUpdate)
        Me.Panel2.Controls.Add(Me.cbxRelease)
        Me.Panel2.Controls.Add(FnotesLabel)
        Me.Panel2.Controls.Add(Me.FnotesTextBox)
        Me.Panel2.Controls.Add(Me.fcustno)
        Me.Panel2.Controls.Add(Me.FcaddrkeyComboBox)
        Me.Panel2.Controls.Add(FmstreetLabel)
        Me.Panel2.Controls.Add(Me.FmstreetLabel1)
        Me.Panel2.Controls.Add(FccompanyLabel)
        Me.Panel2.Controls.Add(Me.FccompanyLabel1)
        Me.Panel2.Controls.Add(ColorLabel)
        Me.Panel2.Controls.Add(Me.ColorComboBox)
        Me.Panel2.Controls.Add(Me.lblDraftRel)
        Me.Panel2.Controls.Add(Me.lblFFMID)
        Me.Panel2.Controls.Add(Me.lblPoAmt)
        Me.Panel2.Controls.Add(Me.lblselectcuft)
        Me.Panel2.Controls.Add(Me.lblprojcuft)
        Me.Panel2.Controls.Add(Me.FprojectTextBox)
        Me.Panel2.Controls.Add(Me.txtPM)
        Me.Panel2.Controls.Add(lblTotSelected)
        Me.Panel2.Controls.Add(lblProjSelected)
        Me.Panel2.Controls.Add(Timestamp_ColumnLabel)
        Me.Panel2.Controls.Add(Me.Timestamp_ColumnLabel1)
        Me.Panel2.Controls.Add(Me.DateTimePicker1)
        Me.Panel2.Controls.Add(lblShippingAddNum)
        Me.Panel2.Controls.Add(Me.cmdLastSo)
        Me.Panel2.Controls.Add(lblPM)
        Me.Panel2.Controls.Add(FcustnoLabel)
        Me.Panel2.Controls.Add(FpoamtLabel)
        Me.Panel2.Controls.Add(FsonoLabel)
        Me.Panel2.Controls.Add(Me.FsonoTextBox)
        Me.Panel2.Controls.Add(Me.cbxAbcCode)
        Me.Panel2.Controls.Add(Me.lblABC)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblCpcft)
        Me.Panel2.Controls.Add(Me.txtCpcft)
        Me.Panel2.Controls.Add(FprojectLabel)
        Me.Panel2.Controls.Add(FFMIDLabel)
        Me.Panel2.Controls.Add(Me.FFMIDComboBox)
        Me.Panel2.Controls.Add(Me.cmdAddSo)
        Me.Panel2.Controls.Add(Me.cmdLoadInmast)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 24)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1234, 213)
        Me.Panel2.TabIndex = 52
        '
        'Fkey_idComboBox
        '
        Me.Fkey_idComboBox.DataSource = Me.M2mFac_ps_xrefBindingSource
        Me.Fkey_idComboBox.DisplayMember = "fkey_id"
        Me.Fkey_idComboBox.FormattingEnabled = True
        Me.Fkey_idComboBox.Location = New System.Drawing.Point(89, 3)
        Me.Fkey_idComboBox.Name = "Fkey_idComboBox"
        Me.Fkey_idComboBox.Size = New System.Drawing.Size(75, 21)
        Me.Fkey_idComboBox.TabIndex = 93
        Me.Fkey_idComboBox.ValueMember = "fkey_id"
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Location = New System.Drawing.Point(1003, 153)
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(75, 23)
        Me.cmdUpdate.TabIndex = 92
        Me.cmdUpdate.Text = "Update SW"
        Me.cmdUpdate.UseVisualStyleBackColor = True
        Me.cmdUpdate.Visible = False
        '
        'cbxRelease
        '
        Me.cbxRelease.DataSource = Me.Sw_releaseBindingSource
        Me.cbxRelease.DisplayMember = "release"
        Me.cbxRelease.FormattingEnabled = True
        Me.cbxRelease.Location = New System.Drawing.Point(294, 29)
        Me.cbxRelease.Name = "cbxRelease"
        Me.cbxRelease.Size = New System.Drawing.Size(50, 21)
        Me.cbxRelease.TabIndex = 90
        Me.cbxRelease.ValueMember = "release"
        '
        'FnotesTextBox
        '
        Me.FnotesTextBox.Enabled = False
        Me.FnotesTextBox.Location = New System.Drawing.Point(752, 155)
        Me.FnotesTextBox.Name = "FnotesTextBox"
        Me.FnotesTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FnotesTextBox.TabIndex = 89
        '
        'fcustno
        '
        Me.fcustno.AutoSize = True
        Me.fcustno.Location = New System.Drawing.Point(597, 30)
        Me.fcustno.Name = "fcustno"
        Me.fcustno.Size = New System.Drawing.Size(42, 13)
        Me.fcustno.TabIndex = 87
        Me.fcustno.Text = "fcustno"
        '
        'FcaddrkeyComboBox
        '
        Me.FcaddrkeyComboBox.DataSource = Me.SyaddrBindingSource
        Me.FcaddrkeyComboBox.DisplayMember = "fcaddrkey"
        Me.FcaddrkeyComboBox.FormattingEnabled = True
        Me.FcaddrkeyComboBox.Location = New System.Drawing.Point(600, 58)
        Me.FcaddrkeyComboBox.Name = "FcaddrkeyComboBox"
        Me.FcaddrkeyComboBox.Size = New System.Drawing.Size(97, 21)
        Me.FcaddrkeyComboBox.TabIndex = 86
        Me.FcaddrkeyComboBox.ValueMember = "fcaddrkey"
        '
        'FmstreetLabel1
        '
        Me.FmstreetLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SyaddrBindingSource, "fmstreet", True))
        Me.FmstreetLabel1.Location = New System.Drawing.Point(597, 114)
        Me.FmstreetLabel1.Name = "FmstreetLabel1"
        Me.FmstreetLabel1.Size = New System.Drawing.Size(350, 31)
        Me.FmstreetLabel1.TabIndex = 85
        Me.FmstreetLabel1.Text = "Label2"
        '
        'FccompanyLabel1
        '
        Me.FccompanyLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SyaddrBindingSource, "fccompany", True))
        Me.FccompanyLabel1.Location = New System.Drawing.Point(597, 90)
        Me.FccompanyLabel1.Name = "FccompanyLabel1"
        Me.FccompanyLabel1.Size = New System.Drawing.Size(347, 17)
        Me.FccompanyLabel1.TabIndex = 84
        Me.FccompanyLabel1.Text = "Label2"
        '
        'ColorComboBox
        '
        Me.ColorComboBox.DataSource = Me.Rockcast_colorsBindingSource
        Me.ColorComboBox.DisplayMember = "color"
        Me.ColorComboBox.FormattingEnabled = True
        Me.ColorComboBox.Location = New System.Drawing.Point(92, 95)
        Me.ColorComboBox.Name = "ColorComboBox"
        Me.ColorComboBox.Size = New System.Drawing.Size(121, 21)
        Me.ColorComboBox.TabIndex = 81
        Me.ColorComboBox.ValueMember = "color_code"
        '
        'lblDraftRel
        '
        Me.lblDraftRel.AutoSize = True
        Me.lblDraftRel.Location = New System.Drawing.Point(226, 32)
        Me.lblDraftRel.Name = "lblDraftRel"
        Me.lblDraftRel.Size = New System.Drawing.Size(62, 13)
        Me.lblDraftRel.TabIndex = 79
        Me.lblDraftRel.Text = "Draft Rel #:"
        '
        'lblFFMID
        '
        Me.lblFFMID.AutoSize = True
        Me.lblFFMID.Location = New System.Drawing.Point(392, 79)
        Me.lblFFMID.Name = "lblFFMID"
        Me.lblFFMID.Size = New System.Drawing.Size(39, 13)
        Me.lblFFMID.TabIndex = 80
        Me.lblFFMID.Text = "Label2"
        Me.lblFFMID.Visible = False
        '
        'lblPoAmt
        '
        Me.lblPoAmt.AutoSize = True
        Me.lblPoAmt.Location = New System.Drawing.Point(89, 79)
        Me.lblPoAmt.Name = "lblPoAmt"
        Me.lblPoAmt.Size = New System.Drawing.Size(39, 13)
        Me.lblPoAmt.TabIndex = 77
        Me.lblPoAmt.Text = "Label2"
        '
        'lblselectcuft
        '
        Me.lblselectcuft.AutoSize = True
        Me.lblselectcuft.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblselectcuft.Location = New System.Drawing.Point(950, 27)
        Me.lblselectcuft.Name = "lblselectcuft"
        Me.lblselectcuft.Size = New System.Drawing.Size(128, 17)
        Me.lblselectcuft.TabIndex = 75
        Me.lblselectcuft.Text = "nothing selected"
        '
        'lblprojcuft
        '
        Me.lblprojcuft.AutoSize = True
        Me.lblprojcuft.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblprojcuft.Location = New System.Drawing.Point(950, 3)
        Me.lblprojcuft.Name = "lblprojcuft"
        Me.lblprojcuft.Size = New System.Drawing.Size(73, 17)
        Me.lblprojcuft.TabIndex = 76
        Me.lblprojcuft.Text = "total proj"
        '
        'FprojectTextBox
        '
        Me.FprojectTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.FMAKERIDBindingSource, "fproject", True))
        Me.FprojectTextBox.Location = New System.Drawing.Point(89, 56)
        Me.FprojectTextBox.MaxLength = 100
        Me.FprojectTextBox.Name = "FprojectTextBox"
        Me.FprojectTextBox.Size = New System.Drawing.Size(359, 20)
        Me.FprojectTextBox.TabIndex = 52
        '
        'txtPM
        '
        Me.txtPM.Location = New System.Drawing.Point(600, 155)
        Me.txtPM.Name = "txtPM"
        Me.txtPM.Size = New System.Drawing.Size(51, 20)
        Me.txtPM.TabIndex = 56
        '
        'Timestamp_ColumnLabel1
        '
        Me.Timestamp_ColumnLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.FMAKERIDBindingSource, "Timestamp_Column", True))
        Me.Timestamp_ColumnLabel1.Location = New System.Drawing.Point(265, 178)
        Me.Timestamp_ColumnLabel1.Name = "Timestamp_ColumnLabel1"
        Me.Timestamp_ColumnLabel1.Size = New System.Drawing.Size(100, 23)
        Me.Timestamp_ColumnLabel1.TabIndex = 74
        Me.Timestamp_ColumnLabel1.Text = "Label2"
        Me.Timestamp_ColumnLabel1.Visible = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(92, 149)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 54
        '
        'cmdLastSo
        '
        Me.cmdLastSo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdLastSo.Location = New System.Drawing.Point(1003, 123)
        Me.cmdLastSo.Name = "cmdLastSo"
        Me.cmdLastSo.Size = New System.Drawing.Size(75, 23)
        Me.cmdLastSo.TabIndex = 59
        Me.cmdLastSo.Text = "Last SO"
        Me.cmdLastSo.UseVisualStyleBackColor = True
        '
        'FsonoTextBox
        '
        Me.FsonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SomastBindingSource, "fsono", True))
        Me.FsonoTextBox.Location = New System.Drawing.Point(600, 3)
        Me.FsonoTextBox.Name = "FsonoTextBox"
        Me.FsonoTextBox.ReadOnly = True
        Me.FsonoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FsonoTextBox.TabIndex = 66
        '
        'cbxAbcCode
        '
        Me.cbxAbcCode.DataSource = Me.LeadtimesBindingSource
        Me.cbxAbcCode.DisplayMember = "description"
        Me.cbxAbcCode.FormattingEnabled = True
        Me.cbxAbcCode.Location = New System.Drawing.Point(92, 122)
        Me.cbxAbcCode.Name = "cbxAbcCode"
        Me.cbxAbcCode.Size = New System.Drawing.Size(273, 21)
        Me.cbxAbcCode.TabIndex = 53
        Me.cbxAbcCode.ValueMember = "abccode"
        '
        'lblABC
        '
        Me.lblABC.AutoSize = True
        Me.lblABC.Location = New System.Drawing.Point(16, 126)
        Me.lblABC.Name = "lblABC"
        Me.lblABC.Size = New System.Drawing.Size(70, 13)
        Me.lblABC.TabIndex = 63
        Me.lblABC.Text = "Project Type:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 62
        Me.Label1.Text = "Delivery Date:"
        '
        'lblCpcft
        '
        Me.lblCpcft.AutoSize = True
        Me.lblCpcft.Location = New System.Drawing.Point(29, 178)
        Me.lblCpcft.Name = "lblCpcft"
        Me.lblCpcft.Size = New System.Drawing.Size(57, 13)
        Me.lblCpcft.TabIndex = 64
        Me.lblCpcft.Text = "Cost/ cuft:"
        '
        'txtCpcft
        '
        Me.txtCpcft.Location = New System.Drawing.Point(92, 175)
        Me.txtCpcft.Name = "txtCpcft"
        Me.txtCpcft.Size = New System.Drawing.Size(48, 20)
        Me.txtCpcft.TabIndex = 55
        Me.txtCpcft.Text = "0"
        '
        'FFMIDComboBox
        '
        Me.FFMIDComboBox.DataSource = Me.FMAKERIDBindingSource
        Me.FFMIDComboBox.DisplayMember = "FFMID"
        Me.FFMIDComboBox.FormattingEnabled = True
        Me.FFMIDComboBox.Location = New System.Drawing.Point(89, 29)
        Me.FFMIDComboBox.Name = "FFMIDComboBox"
        Me.FFMIDComboBox.Size = New System.Drawing.Size(121, 21)
        Me.FFMIDComboBox.TabIndex = 51
        Me.FFMIDComboBox.ValueMember = "FFMID"
        '
        'cmdAddSo
        '
        Me.cmdAddSo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdAddSo.Location = New System.Drawing.Point(1003, 94)
        Me.cmdAddSo.Name = "cmdAddSo"
        Me.cmdAddSo.Size = New System.Drawing.Size(75, 23)
        Me.cmdAddSo.TabIndex = 58
        Me.cmdAddSo.Text = "Add So"
        Me.cmdAddSo.UseVisualStyleBackColor = True
        '
        'cmdLoadInmast
        '
        Me.cmdLoadInmast.Location = New System.Drawing.Point(997, 99)
        Me.cmdLoadInmast.Name = "cmdLoadInmast"
        Me.cmdLoadInmast.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoadInmast.TabIndex = 57
        Me.cmdLoadInmast.Text = "Load Inmast"
        Me.cmdLoadInmast.UseVisualStyleBackColor = True
        Me.cmdLoadInmast.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SwmastDataGridView)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 237)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1234, 547)
        Me.Panel1.TabIndex = 53
        '
        'SwmastDataGridView
        '
        Me.SwmastDataGridView.AllowUserToAddRows = False
        Me.SwmastDataGridView.AllowUserToDeleteRows = False
        Me.SwmastDataGridView.AllowUserToOrderColumns = True
        Me.SwmastDataGridView.AutoGenerateColumns = False
        Me.SwmastDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SwmastDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgAdd, Me.dgPieceMark, Me.dgQuoteQty, Me.dgStarted, Me.dgOpen, Me.dgClosed, Me.dgOnHold, Me.dgCancelled, Me.dgShipped, Me.dgAddQty, Me.dgFnotes, Me.dgCUFT, Me.dgColor, Me.dgRelease, Me.dgFpartno, Me.dgOrdQty, Me.FfmidDataGridViewTextBoxColumn, Me.SysdateDataGridViewTextBoxColumn, Me.dgWeight, Me.IdentityColumnDataGridViewTextBoxColumn, Me.dgReason, Me.dgDrawno, Me.verified, Me.sawcut, Me.extract_path, Me.pc})
        Me.SwmastDataGridView.DataMember = "swmast"
        Me.SwmastDataGridView.DataSource = Me.DsSWM2M
        Me.SwmastDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SwmastDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.SwmastDataGridView.Name = "SwmastDataGridView"
        Me.SwmastDataGridView.Size = New System.Drawing.Size(1234, 547)
        Me.SwmastDataGridView.TabIndex = 17
        '
        'dgAdd
        '
        Me.dgAdd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAdd.DataPropertyName = "add"
        Me.dgAdd.Frozen = True
        Me.dgAdd.HeaderText = "Add"
        Me.dgAdd.Name = "dgAdd"
        Me.dgAdd.Width = 32
        '
        'dgPieceMark
        '
        Me.dgPieceMark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgPieceMark.DataPropertyName = "peice_mark"
        Me.dgPieceMark.Frozen = True
        Me.dgPieceMark.HeaderText = "Piece Mark"
        Me.dgPieceMark.Name = "dgPieceMark"
        Me.dgPieceMark.ReadOnly = True
        Me.dgPieceMark.Width = 86
        '
        'dgQuoteQty
        '
        Me.dgQuoteQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgQuoteQty.DataPropertyName = "fquoteqty"
        Me.dgQuoteQty.HeaderText = "Layout Qty"
        Me.dgQuoteQty.Name = "dgQuoteQty"
        Me.dgQuoteQty.ReadOnly = True
        Me.dgQuoteQty.Width = 83
        '
        'dgStarted
        '
        Me.dgStarted.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgStarted.DataPropertyName = "started"
        DataGridViewCellStyle1.Format = "n0"
        Me.dgStarted.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgStarted.HeaderText = "Started"
        Me.dgStarted.Name = "dgStarted"
        Me.dgStarted.ReadOnly = True
        Me.dgStarted.Width = 66
        '
        'dgOpen
        '
        Me.dgOpen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOpen.DataPropertyName = "open"
        DataGridViewCellStyle2.Format = "n0"
        Me.dgOpen.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgOpen.HeaderText = "Open"
        Me.dgOpen.Name = "dgOpen"
        Me.dgOpen.ReadOnly = True
        Me.dgOpen.Width = 58
        '
        'dgClosed
        '
        Me.dgClosed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgClosed.DataPropertyName = "closed"
        DataGridViewCellStyle3.Format = "n0"
        Me.dgClosed.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgClosed.HeaderText = "Closed"
        Me.dgClosed.Name = "dgClosed"
        Me.dgClosed.ReadOnly = True
        Me.dgClosed.Width = 64
        '
        'dgOnHold
        '
        Me.dgOnHold.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOnHold.DataPropertyName = "onhold"
        DataGridViewCellStyle4.Format = "n0"
        Me.dgOnHold.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgOnHold.HeaderText = "On Hold"
        Me.dgOnHold.Name = "dgOnHold"
        Me.dgOnHold.ReadOnly = True
        Me.dgOnHold.Width = 71
        '
        'dgCancelled
        '
        Me.dgCancelled.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgCancelled.DataPropertyName = "cancelled"
        DataGridViewCellStyle5.Format = "n0"
        Me.dgCancelled.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgCancelled.HeaderText = "Cancelled"
        Me.dgCancelled.Name = "dgCancelled"
        Me.dgCancelled.ReadOnly = True
        Me.dgCancelled.Width = 79
        '
        'dgShipped
        '
        Me.dgShipped.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgShipped.DataPropertyName = "shipped"
        DataGridViewCellStyle6.Format = "n0"
        Me.dgShipped.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgShipped.HeaderText = "Shipped"
        Me.dgShipped.Name = "dgShipped"
        Me.dgShipped.ReadOnly = True
        Me.dgShipped.Width = 71
        '
        'dgAddQty
        '
        Me.dgAddQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAddQty.DataPropertyName = "add_qty"
        DataGridViewCellStyle7.Format = "n0"
        Me.dgAddQty.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgAddQty.HeaderText = "Qty to Add"
        Me.dgAddQty.Name = "dgAddQty"
        Me.dgAddQty.Width = 82
        '
        'dgFnotes
        '
        Me.dgFnotes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFnotes.DataPropertyName = "fnotes"
        Me.dgFnotes.HeaderText = "Notes"
        Me.dgFnotes.Name = "dgFnotes"
        Me.dgFnotes.ReadOnly = True
        Me.dgFnotes.Width = 60
        '
        'dgCUFT
        '
        Me.dgCUFT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgCUFT.DataPropertyName = "fcuft"
        Me.dgCUFT.HeaderText = "CUFT"
        Me.dgCUFT.Name = "dgCUFT"
        Me.dgCUFT.ReadOnly = True
        Me.dgCUFT.Width = 60
        '
        'dgColor
        '
        Me.dgColor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgColor.DataPropertyName = "color"
        Me.dgColor.HeaderText = "Color"
        Me.dgColor.Name = "dgColor"
        Me.dgColor.ReadOnly = True
        Me.dgColor.Width = 56
        '
        'dgRelease
        '
        Me.dgRelease.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgRelease.DataPropertyName = "release"
        Me.dgRelease.HeaderText = "Release"
        Me.dgRelease.Name = "dgRelease"
        Me.dgRelease.ReadOnly = True
        Me.dgRelease.Width = 71
        '
        'dgFpartno
        '
        Me.dgFpartno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFpartno.DataPropertyName = "fpartno"
        Me.dgFpartno.HeaderText = "Part"
        Me.dgFpartno.Name = "dgFpartno"
        Me.dgFpartno.ReadOnly = True
        Me.dgFpartno.Width = 51
        '
        'dgOrdQty
        '
        Me.dgOrdQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOrdQty.DataPropertyName = "forderqty"
        Me.dgOrdQty.HeaderText = "forderqty"
        Me.dgOrdQty.Name = "dgOrdQty"
        Me.dgOrdQty.ReadOnly = True
        Me.dgOrdQty.Visible = False
        '
        'FfmidDataGridViewTextBoxColumn
        '
        Me.FfmidDataGridViewTextBoxColumn.DataPropertyName = "ffmid"
        Me.FfmidDataGridViewTextBoxColumn.HeaderText = "ffmid"
        Me.FfmidDataGridViewTextBoxColumn.Name = "FfmidDataGridViewTextBoxColumn"
        Me.FfmidDataGridViewTextBoxColumn.Visible = False
        '
        'SysdateDataGridViewTextBoxColumn
        '
        Me.SysdateDataGridViewTextBoxColumn.DataPropertyName = "sys_date"
        Me.SysdateDataGridViewTextBoxColumn.HeaderText = "sys_date"
        Me.SysdateDataGridViewTextBoxColumn.Name = "SysdateDataGridViewTextBoxColumn"
        Me.SysdateDataGridViewTextBoxColumn.Visible = False
        '
        'dgWeight
        '
        Me.dgWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgWeight.DataPropertyName = "weight"
        Me.dgWeight.HeaderText = "weight"
        Me.dgWeight.Name = "dgWeight"
        Me.dgWeight.ReadOnly = True
        Me.dgWeight.Visible = False
        '
        'IdentityColumnDataGridViewTextBoxColumn
        '
        Me.IdentityColumnDataGridViewTextBoxColumn.DataPropertyName = "Identity_Column"
        Me.IdentityColumnDataGridViewTextBoxColumn.HeaderText = "Identity_Column"
        Me.IdentityColumnDataGridViewTextBoxColumn.Name = "IdentityColumnDataGridViewTextBoxColumn"
        Me.IdentityColumnDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdentityColumnDataGridViewTextBoxColumn.Visible = False
        '
        'dgReason
        '
        Me.dgReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgReason.DataPropertyName = "reason"
        Me.dgReason.HeaderText = "Reason"
        Me.dgReason.MaxInputLength = 250
        Me.dgReason.Name = "dgReason"
        Me.dgReason.Width = 69
        '
        'dgDrawno
        '
        Me.dgDrawno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgDrawno.DataPropertyName = "fdrawno"
        Me.dgDrawno.HeaderText = "Draw No"
        Me.dgDrawno.Name = "dgDrawno"
        Me.dgDrawno.ReadOnly = True
        Me.dgDrawno.Visible = False
        '
        'verified
        '
        Me.verified.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.verified.DataPropertyName = "verified"
        Me.verified.HeaderText = "verified"
        Me.verified.Name = "verified"
        Me.verified.ReadOnly = True
        Me.verified.Width = 47
        '
        'sawcut
        '
        Me.sawcut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.sawcut.DataPropertyName = "sawcut"
        Me.sawcut.HeaderText = "sawcut"
        Me.sawcut.Name = "sawcut"
        Me.sawcut.Width = 47
        '
        'extract_path
        '
        Me.extract_path.DataPropertyName = "extract_path"
        Me.extract_path.HeaderText = "extract_path"
        Me.extract_path.Name = "extract_path"
        Me.extract_path.Visible = False
        '
        'pc
        '
        Me.pc.DataPropertyName = "pc mk #"
        Me.pc.HeaderText = "pc mk #"
        Me.pc.Name = "pc"
        Me.pc.Visible = False
        '
        'frmSWtoM2M
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1234, 784)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmSWtoM2M"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sales Order Staging"
        CType(Me.FMAKERIDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsSWM2M, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeadtimesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SomastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.Rockcast_colorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SyaddrBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sw_releaseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.M2mFac_ps_xrefBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SoitemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InmastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvcurBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SlcdpmBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SorelsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SotaxBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SOMAST_EXTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.All_so_qtysBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Email_listBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SwmastBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.App_errorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.SwmastDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DsSWM2M As SW_to_M2M_SO.dsSWM2M
    Friend WithEvents FMAKERIDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FMAKERIDTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.FMAKERIDTableAdapter
    Friend WithEvents TableAdapterManager As SW_to_M2M_SO.dsSWM2MTableAdapters.TableAdapterManager
    Friend WithEvents SwmastTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.swmastTableAdapter
    Friend WithEvents InmastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InmastTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.inmastTableAdapter
    Friend WithEvents Rockcast_colorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Rockcast_colorsTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.rockcast_colorsTableAdapter
    Friend WithEvents InvcurTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.invcurTableAdapter
    Friend WithEvents InvcurBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SomastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SomastTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.somastTableAdapter
    Friend WithEvents SoitemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SoitemTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.soitemTableAdapter
    Friend WithEvents SlcdpmBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SlcdpmTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.slcdpmTableAdapter
    Friend WithEvents SorelsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SorelsTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.sorelsTableAdapter
    Friend WithEvents SotaxTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.sotaxTableAdapter
    Friend WithEvents SotaxBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SOMAST_EXTTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.SOMAST_EXTTableAdapter
    Friend WithEvents SOMAST_EXTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents All_so_qtysBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents All_so_qtysTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.all_so_qtysTableAdapter
    Friend WithEvents Email_listBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Email_listTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.email_listTableAdapter
    Friend WithEvents SyaddrTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.syaddrTableAdapter
    Friend WithEvents SyaddrBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LeadtimesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutSWToM2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SwmastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sw_releaseBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sw_releaseTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.sw_releaseTableAdapter
    Friend WithEvents M2mFac_ps_xrefBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents M2mFac_ps_xrefTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.m2mFac_ps_xrefTableAdapter
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReconfigDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents M2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackingSolutionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents App_errorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents App_errorsTableAdapter As SW_to_M2M_SO.dsSWM2MTableAdapters.app_errorsTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Fkey_idComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmdUpdate As System.Windows.Forms.Button
    Friend WithEvents cbxRelease As System.Windows.Forms.ComboBox
    Friend WithEvents FnotesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents fcustno As System.Windows.Forms.Label
    Friend WithEvents FcaddrkeyComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents FmstreetLabel1 As System.Windows.Forms.Label
    Friend WithEvents FccompanyLabel1 As System.Windows.Forms.Label
    Friend WithEvents ColorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents lblDraftRel As System.Windows.Forms.Label
    Friend WithEvents lblFFMID As System.Windows.Forms.Label
    Friend WithEvents lblPoAmt As System.Windows.Forms.Label
    Friend WithEvents lblselectcuft As System.Windows.Forms.Label
    Friend WithEvents lblprojcuft As System.Windows.Forms.Label
    Friend WithEvents FprojectTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtPM As System.Windows.Forms.TextBox
    Friend WithEvents Timestamp_ColumnLabel1 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdLastSo As System.Windows.Forms.Button
    Friend WithEvents FsonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cbxAbcCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblABC As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCpcft As System.Windows.Forms.Label
    Friend WithEvents txtCpcft As System.Windows.Forms.TextBox
    Friend WithEvents FFMIDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddSo As System.Windows.Forms.Button
    Friend WithEvents cmdLoadInmast As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SwmastDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents dgAdd As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgPieceMark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgQuoteQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgStarted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgOpen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgClosed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgOnHold As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgCancelled As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgShipped As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgAddQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFnotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgCUFT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgRelease As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFpartno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgOrdQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FfmidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SysdateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdentityColumnDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgDrawno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents verified As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents sawcut As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents extract_path As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pc As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
