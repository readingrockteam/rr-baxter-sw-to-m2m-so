﻿determine the cost per cuft.
first look at the color from solidworks.  Determine # of blends.  (need # blends look up table.)
reference std pricing based on number of blends.
Allow over ride on a per item basis.  (need a new column populate with $/cuft)

Provide the ablility to distribute the remaining monies over selected parts that are being added to "last SO"

Provide ability to apply a % markup over selected parts and recalculate the total value of the new SO

Prevent exceeding the total value of the original quote in Filemaker.

Prevent exceeding the total quantities of the original layout on a per part basis.

select * from somast where fsono IN ('151546', 'S00005')
select * from soitem where fsono IN ('151546', 'S00005')
select * from sodbom where fsono IN ('151546', 'S00005')
select * from sochng where fsono IN ('151755', 'S00005')
select * from sorels where fsono IN ('151755', 'S00005')
select * from sorcom where fsono IN ('151755', 'S00005')
select * from sotax where fsono IN ('151755', 'S00005')
select * from somast_ext inner join somast on somast.identity_column = somast_ext.FKey_ID where somast.fsono = 'S00004'
select * from soitem_ext inner join soitem on soitem.identity_column = soitem_ext.FKey_ID where soitem.fsono = 'S00004'
select * from slcdpm inner join somast on somast.fcustno = slcdpm.fcustno where somast.fsono = 'S00004'
select * from incntr

select syscolumns.name from syscolumns inner join sysobjects on syscolumns.id = sysobjects.id where sysobjects.name like 'SOMAST_EXT'
--select sysobjects.name from sysobjects inner join syscolumns on syscolumns.id = sysobjects.id where syscolumns.name = 'FCUSTNO'